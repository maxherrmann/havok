import numpy

from scipy.integrate import solve_ivp as solver

import matplotlib.pyplot as plotter
import matplotlib.animation as animation

import lagrange_multiplier as L
import rhs_lqr

def deg2rad(arg):
    return arg * numpy.pi / 180

# rotation matrix
def T(phi):
    return numpy.array([[numpy.cos(phi), -numpy.sin(phi)], [numpy.sin(phi), numpy.cos(phi)]])

# shoulder torque
def M1(t, x, xs, k1, ll, ul):
    if t < 12:
        if t > 5:
            return -1.2
        return t/5 * (-1.2)
    out = -1.2 - k1 @ (x - xs).T
    out = max(out, ll)
    out = min(out, ul)    
    return out
    
def M2(t, x, xs, k2, ll, ul):
    if t < 12:
        if t > 5:
            return 2.3
        return t/5 * (2.3)
    out = 2.3 - k2 @ (x - xs).T
    out = max(out, ll)
    out = min(out, ul)
    return out
    
def Fsupx(t):
    if t > 14:
        if t > 14.2:
            return 0
        return 0.9
    return 0
    
def Fsupz(t):
    return 0

# get Lagrange multiplier expression, and constraint forces
lambda1_val, lambda2_val, cf_x1, cf_z1, cf_x2, cf_z2 = L.compute_lagrange_multipliers()

# LQR gains
# Q = eye(8);
# R = 2 * eye(2);
# N = zeros(8, 2);
k1 = numpy.array([4.2688,   52.2841,   -0.7986, -113.2035,   -2.1666,  -13.6122,    0.1229,   11.5961])
k2 = numpy.array([-41.5224, -227.0663,   39.0398,  179.9405,   23.7316,  116.1131,  -19.3953,  -95.6565])

# steady state
xs = [0.0359, 0, -0.1457, 0, 0.1868, 0, -0.3876, 0]

# parameter values
l1 = 0.3
l2 = 0.3
d1 = -0.4
d2 = -0.4
m1 = 3
m2 = 2
g  = 10

# upper and lower limits of control input
ul1 = 1000
ll1 = -1000
ul2 = 1000
ll2 = -1000

# parameters that need to be passed to the right-hand side
parameters = (lambda1_val, lambda2_val, cf_x1, cf_z1, cf_x2, cf_z2, M1, M2, Fsupx, Fsupz, {'l1': l1, 'l2': l2, 'm1': m1, 'm2': m2, 'g': g, 'd1': d1, 'd2': d2, 'xs': xs, 'k1': k1, 'k2': k2, 'ul1': ul1, 'll1': ll1, 'ul2': ul2, 'll2': ll2 })

# integrate over 10 time units, every 0.01 time units
dt = 0.01
t_end = 20
t_span = (0, t_end)
time_points = numpy.arange(0, t_end, dt)
number_of_time_points = len(time_points)

# initial conditions
phi1 = deg2rad(110)
phi2 = deg2rad(140)
x1 = T(phi1) @ numpy.array([[0], [-1]]) * l1/2
x2 = 2 * x1 + T(phi2) @ numpy.array([[0], [-1]]) * l2 / 2
x0 = [0, 0, -l1/2, 0, l2/2, 0, -l1, 0]
x0 = [x1[0], 0, x1[1], 0, x2[0], 0, x2[1], 0]
x0 = [0, 0, -l1/2, 0, 0, 0, -(l1 + l2/2), 0]

# solve initial-value problem
integration_method = 'Radau'
solution = solver(rhs_lqr.f, t_span, x0, args=parameters, t_eval=time_points, method=integration_method)

# get mass 1 and 2 positions
x1 = solution.y[0]
z1 = solution.y[2]
x2 = solution.y[4]
z2 = solution.y[6]

# plot results
l = l1 + l2
figure = plotter.figure(figsize = (5,4))
axis = figure.add_subplot(autoscale_on=False, xlim=(-l, l), ylim=(-l, l))
axis.set_aspect('equal')

limbs, = axis.plot([], [])
mass_1, = axis.plot([], [], 'o', color='red')
mass_2, = axis.plot([], [], 'o', color='red')

plotter.figure()
plotter.plot(time_points, x2)

# redraw limbs and masses at each time step
def animate(frame_index):
    
    # draw limbs
    limbs.set_data(
       [0, 2*x1[frame_index], 2*(x2[frame_index] - x1[frame_index])], 
       [0, 2*z1[frame_index], 2*(z2[frame_index] - z1[frame_index])])
       
    # draw mass 1
    mass_1.set_data(x1[frame_index], z1[frame_index])
    
    # draw mass 2
    mass_2.set_data(x2[frame_index], z2[frame_index])
    
    return limbs, mass_1, mass_2
    
animation_object = animation.FuncAnimation(figure, animate, number_of_time_points, interval=dt*1000, blit=True)

plotter.show()