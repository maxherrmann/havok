# read XSENSOR pressure measurements of ExoMePT test parcour:
# Estimated load is 2nd to last column "Est. Load (N)" in file
#
# ../data/XSENSOR_pressure_mat/A1C2.csv
#
# Measurement frequency is approx. 36 Hz.

from numpy import array
from numpy import amax

import csv

def read_file(path):

    # open file with line endings returned to the caller untranslated
    file = open(path, 'r', newline='')

    # instantiate a csv reader
    csv_reader = csv.reader(file, delimiter=';')

    # read contents of csv file, and write it to list
    # https://docs.python.org/3/library/csv.html
    # "Each row read from the csv file is returned as a list of strings."
    data = []
    line_index = 0
    for line in csv_reader:
        # ignore header lines
        if line_index < 3:
            line_index += 1
            continue
        data.append([])
        for entry in line:
            data[-1].append(entry)
        line_index += 1

    # close file
    file.close()
 
    # collect 2nd to last entry only
    load = []
    for line in data:
        load.append(float(line[-2]))
        
    return load
    
def normalize_data(data):
    
    max_value = amax(data)
    return data / max_value
    
def preprocess_input_data(path, start, end):
    
    # read csv data from file
    raw_input_data = read_file(path)

    # crop data
    cropped_input_data = raw_input_data[start:end]

    # normalize data
    normalized_cropped_input_data = normalize_data(array(cropped_input_data))
    
    return normalized_cropped_input_data