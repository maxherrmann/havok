import numpy

def f(t, x, lambda1f, lambda2f, cf_x1f, cf_z1f, cf_x2f, cf_z2f, M1f, M2f, Fsupxf, Fsupzf, u, parameters):

    # states
    # ------
    x1  = x[0]
    dx1 = x[1]
    z1  = x[2]
    dz1 = x[3]
    x2  = x[4]
    dx2 = x[5]
    z2  = x[6]
    dz2 = x[7]
    
    # parameters
    # ----------
    l1 = parameters['l1']
    l2 = parameters['l2']
    m1 = parameters['m1']
    m2 = parameters['m2']
    g  = parameters['g']
    d1 = parameters['d1']
    d2 = parameters['d2']
    k1 = parameters['k1']
    k2 = parameters['k2']
    xs = parameters['xs']    
    
    # compute torque
    M1 = M1f(t, x, xs, k1)
    M2 = M2f(t, x, xs, k2)
    
    # compute support
    nx = (-z1)/(l1/2)
    nz = x1/(l1/2)
    Fsupx = Fsupxf(t, nx, u['t'], u['y'])
    Fsupz = Fsupzf(t, nz, u['t'], u['y'])
    
    # compute Lagrange multipliers
    lambda1 = lambda1f(Fsupx, Fsupz, M1, M2, x1, dx1, z1, dz1, x2, dx2, z2, dz2, d1, d2, l1, l2, m1, m2, g)
    lambda2 = lambda2f(Fsupx, Fsupz, M1, M2, x1, dx1, z1, dz1, x2, dx2, z2, dz2, d1, d2, l1, l2, m1, m2, g)
    
    # cosines and sines of angle as a function of coordinates
    cosphi1 = (-z1) / (l1/2)
    sinphi1 = -(x1 / (l1/2))
    cosphi2 = -(z2 - 2*z1) / (l2/2)
    sinphi2 = -(x2 - 2*x1) / (l2/2)

    # rotation matrices
    T1 = numpy.array([[cosphi1, sinphi1], [-sinphi1, cosphi1]])
    T2 = numpy.array([[cosphi2, sinphi2], [-sinphi2, cosphi2]])

    # torque-induced forces
    FM1 = T1 @ numpy.array([[M1 / (l1/2)], [0]])
    FM1x = FM1[0]
    FM1z = FM1[1]
    FM2 = T2 @ numpy.array([[M2 / (l2 / 2)], [0]])
    FM2x = FM2[0]
    FM2z = FM2[1]
    
    # cosines and sines of angular velocity as a function of coordinates
    cosdphi1 = (-dz1) / (l1/2)
    sindphi1 = -(dx1 / (l1/2))
    cosdphi2 = -(dz2 - 2*dz1) / (l2/2)
    sindphi2 = -(dx2 - 2*dx1) / (l2/2)
    
    # damping forces
    Fd1x = -d1 * sindphi1
    Fd1z = -d1 * cosdphi1
    Fd2x = -d2 * sindphi2
    Fd2z = -d2 * cosdphi2    
    
    # gravitational force
    Fg1z = -m1 * g
    Fg2z = -m2 * g
    
    # constraint forces
    cf_x1 = cf_x1f(x1, z1, x2, z2, l1, l2, lambda1, lambda2)
    cf_z1 = cf_z1f(x1, z1, x2, z2, l1, l2, lambda1, lambda2)
    cf_x2 = cf_x2f(x1, z1, x2, z2, l1, l2, lambda1, lambda2)
    cf_z2 = cf_z2f(x1, z1, x2, z2, l1, l2, lambda1, lambda2)    

    # laws of motion (right-hand side)
    ddx1 = (FM1x + Fd1x + Fsupx + cf_x1) / m1
    ddz1 = (FM1z + Fd1z + Fsupz + Fg1z + cf_z1) / m1
    ddx2 = (FM2x + Fd2x + cf_x2) / m2
    ddz2 = (FM2z + Fd2z + Fg2z + cf_z2) / m2
    
    return [dx1, ddx1, dz1, ddz1, dx2, ddx2, dz2, ddz2]