import math

import numpy

def deg2rad(arg):
    return arg * math.pi / 180

# ..
# m2 * x2 = -u2 * (z2 - 2 * z1) / (l2/2)^2
#
# ..
# m2 * z2 = u2 * (x2 - 2 * x1) / (l2/2)^2
def test_u2(phi1_deg, phi2_deg, u2, m2, l1, l2):

    phi1_rad = deg2rad(phi1_deg)
    phi2_rad = deg2rad(phi2_deg)

    x1 = math.sin(phi1_rad) * l1/2
    z1 = -math.cos(phi1_rad) * l1/2
    
    x2 = 2 * x1 + math.sin(phi2_rad) * l2/2
    z2 = -2 * z1 - math.cos(phi2_rad) * l2/2
    
    M2 = u2
    
    F2x = -math.sin(phi1_rad) * (-math.sin(phi2_rad) * M2 / (l2/2)) + math.cos(phi1_rad) * (math.cos(phi2_rad) * M2 / (l2/2))
    F2z = math.cos(phi1_rad) * (-math.sin(phi2_rad) * M2 / (l2/2)) + math.sin(phi1_rad) * (math.cos(phi2_rad) * M2 / (l2/2))

    return [F2x, F2z]
    
def rotate_45deg_twice():

    point = numpy.array([1, 0])
    
    phi = deg2rad(45)
    
    T = numpy.array([[math.cos(phi), math.sin(phi)], [-math.sin(phi), math.cos(phi)]])
    
    return numpy.matmul(T, numpy.matmul(T, point))