import sympy
import numpy

def compute_jacobian_at_stationary_point(ys, M1s, M2s, Fsupxs, Fsupzs, parameters):

    # define symbols
    f1t = sympy.symbols('f1t', cls=sympy.Function)
    f2t = sympy.symbols('f2t', cls=sympy.Function)
    f1, f2 = sympy.symbols('f1, f2')
    t = sympy.symbols('t')
    x1t, z1t, x2t, z2t = sympy.symbols('x1t, z1t, x2t, z2t', cls=sympy.Function)
    x1, z1, x2, z2 = sympy.symbols('x1, z1, x2, z2')
    dx1, dz1, dx2, dz2 = sympy.symbols('dx1, dz1, dx2, dz2')
    l1, l2, m1, m2, g = sympy.symbols('l1, l2, m1, m2, g')
    cosphi1, sinphi1, cosphi2, sinphi2 = sympy.symbols('cosphi1, sinphi1, cosphi2, sinphi2')
    # applied forces
    M1, M2 = sympy.symbols('M1, M2')
    FM1, FM2, FM1x, FM1z, FM2x, FM2z = sympy.symbols('FM1, FM2, FM1x, FM1z, FM2x, FM2z')
    d1, d2 = sympy.symbols('d1, d2')
    Fd1x, Fd1z, Fd2x, Fd2z = sympy.symbols('Fd1x, Fd1z, Fd2x, Fd2z')
    Fg1z, Fg2z = sympy.symbols('Fg1x, Fg2z')
    Fsupx, Fsupz = sympy.symbols('Fsupx, Fsupz')
    # constraint forces
    lambda1, lambda2 = sympy.symbols('lambda1, lambda2')
    sum_of_all_constraint_forces_x1 = sympy.symbols('sum_of_all_constraint_forces_x1')
    sum_of_all_constraint_forces_z1 = sympy.symbols('sum_of_all_constraint_forces_z1')
    sum_of_all_constraint_forces_x2 = sympy.symbols('sum_of_all_constraint_forces_x2')
    sum_of_all_constraint_forces_z2 = sympy.symbols('sum_of_all_constraint_forces_z2')
    # states
    y1, y2, y3, y4, y5, y6, y7, y8 = sympy.symbols('y1, y2, y3, y4, y5, y6, y7, y8')

    # define constraints
    f1t = x1t(t)**2 + z1t(t)**2 - (l1/2)**2
    f2t = (x2t(t) - 2 * x1t(t))**2 + (z2t(t) - 2 * z1t(t))**2 - (l2/2)**2

    # 2nd derivative over time of constraints
    DDt_f1 = sympy.diff(f1t, t, 2)
    DDt_f2 = sympy.diff(f2t, t, 2)

    # cosines and sines of angle as a function of coordinates
    cosphi1 = (-z1) / (l1/2)
    sinphi1 = -(x1 / (l1/2))
    cosphi2 = -(z2 - 2*z1) / (l2/2)
    sinphi2 = -(x2 - 2*x1) / (l2/2)

    # rotation matrices
    T1 = sympy.Matrix([[cosphi1, sinphi1], [-sinphi1, cosphi1]])
    T2 = sympy.Matrix([[cosphi2, sinphi2], [-sinphi2, cosphi2]])

    # torque-induced forces
    FM1 = T1 * sympy.Matrix([M1 / (l1/2), 0])
    FM1x = FM1[0]
    FM1z = FM1[1]
    FM2 = T2 * sympy.Matrix([M2 / (l2/2), 0])
    FM2x = FM2[0]
    FM2z = FM2[1]
    
    # cosines and sines of angular velocity as a function of coordinates
    cosdphi1 = (-dz1) / (l1/2)
    sindphi1 = -(dx1 / (l1/2))
    cosdphi2 = -(dz2 - 2*dz1) / (l2/2)
    sindphi2 = -(dx2 - 2*dx1) / (l2/2)

    # damping forces
    Fd1x = -d1 * sindphi1
    Fd1z = -d1 * cosdphi1
    Fd2x = -d2 * sindphi2
    Fd2z = -d2 * cosdphi2

    # gravitational force
    Fg1z = -m1 * g
    Fg2z = -m2 * g

    # constraint forces
    f1 = f1t.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])
    f2 = f2t.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])
    df1dx1 = sympy.diff(f1, x1, 1)
    df2dx1 = sympy.diff(f2, x1, 1)
    df1dz1 = sympy.diff(f1, z1, 1)
    df2dz1 = sympy.diff(f2, z1, 1)  
    df1dx2 = sympy.diff(f1, x2, 1)
    df2dx2 = sympy.diff(f2, x2, 1)
    df1dz2 = sympy.diff(f1, z2, 1)
    df2dz2 = sympy.diff(f2, z2, 1)
    sum_of_all_constraint_forces_x1 = lambda1 * df1dx1 + lambda2 * df2dx1
    sum_of_all_constraint_forces_z1 = lambda1 * df1dz1 + lambda2 * df2dz1
    sum_of_all_constraint_forces_x2 = lambda1 * df1dx2 + lambda2 * df2dx2
    sum_of_all_constraint_forces_z2 = lambda1 * df1dz2 + lambda2 * df2dz2

    # laws of motion (right-hand side)
    DDt_x1 = (FM1x + Fd1x + Fsupx + sum_of_all_constraint_forces_x1) / m1
    DDt_z1 = (FM1z + Fd1z + Fsupz + Fg1z + sum_of_all_constraint_forces_z1) / m1
    DDt_x2 = (FM2x + Fd2x + sum_of_all_constraint_forces_x2) / m2
    DDt_z2 = (FM2z + Fd2z + Fg2z + sum_of_all_constraint_forces_z2) / m2

    # substitute 2nd total time derivatives with laws of motion
    DDt_f1 = DDt_f1.subs(
         [(sympy.diff(x1t(t), t, 2), DDt_x1), (sympy.diff(z1t(t), t, 2), DDt_z1), (sympy.diff(x2t(t), t, 2), DDt_x2), (sympy.diff(z2t(t), t, 2), DDt_z2)]
         )
    DDt_f2 = DDt_f2.subs(
         [(sympy.diff(x1t(t), t, 2), DDt_x1), (sympy.diff(z1t(t), t, 2), DDt_z1), (sympy.diff(x2t(t), t, 2), DDt_x2), (sympy.diff(z2t(t), t, 2), DDt_z2)]
         )
         
    # substitute functions with symbols
    DDt_f1 = DDt_f1.subs([(sympy.diff(x1t(t), t, 1), dx1), (sympy.diff(z1t(t), t, 1), dz1), (sympy.diff(x2t(t), t, 1), dx2), (sympy.diff(z2t(t), t, 1), dz2)])
    DDt_f1 = DDt_f1.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])
    DDt_f2 = DDt_f2.subs([(sympy.diff(x1t(t), t, 1), dx1), (sympy.diff(z1t(t), t, 1), dz1), (sympy.diff(x2t(t), t, 1), dx2), (sympy.diff(z2t(t), t, 1), dz2)])
    DDt_f2 = DDt_f2.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])

    # solve for Lagrange multipliers
    lambda12 = sympy.linsolve([DDt_f1, DDt_f2], (lambda1, lambda2))

    # collect components of solution vector
    lambdas = []
    for solutions in lambda12:
        for solution in solutions:
            lambdas.append(solution)
            
    # substitute Lagrange multipliers with expression in laws of motion
    DDt_x1 = DDt_x1.subs([(lambda1, lambdas[0]), (lambda2, lambdas[1])])
    DDt_z1 = DDt_z1.subs([(lambda1, lambdas[0]), (lambda2, lambdas[1])])
    DDt_x2 = DDt_x2.subs([(lambda1, lambdas[0]), (lambda2, lambdas[1])])
    DDt_z2 = DDt_z2.subs([(lambda1, lambdas[0]), (lambda2, lambdas[1])])
    
    # substitute states
    DDt_x1 = DDt_x1.subs([(x1, y1), (dx1, y2), (z1, y3), (dz1, y4), (x2, y5), (dx2, y6), (z2, y7), (dz2, y8)])
    DDt_z1 = DDt_z1.subs([(x1, y1), (dx1, y2), (z1, y3), (dz1, y4), (x2, y5), (dx2, y6), (z2, y7), (dz2, y8)])
    DDt_x2 = DDt_x2.subs([(x1, y1), (dx1, y2), (z1, y3), (dz1, y4), (x2, y5), (dx2, y6), (z2, y7), (dz2, y8)])
    DDt_z2 = DDt_z2.subs([(x1, y1), (dx1, y2), (z1, y3), (dz1, y4), (x2, y5), (dx2, y6), (z2, y7), (dz2, y8)])
    
    # form vector-valued function and vector of independent variables
    f = sympy.Matrix([y2, DDt_x1, y4, DDt_z1, y6, DDt_x2, y8, DDt_z2])
    y = sympy.Matrix([y1, y2, y3, y4, y5, y6, y7, y8])
    u = sympy.Matrix([M1, M2])

    # compute jacobian with respect to coordinates y
    jacobian_A = f.jacobian(y)
    
    # compute jacobian with respect to inputs u
    jacobian_B = f.jacobian(u)
    
    l1_val = parameters['l1']
    l2_val = parameters['l2']
    d1_val = parameters['d1']
    d2_val = parameters['d2']
    m1_val = parameters['m1']
    m2_val = parameters['m2']
    g_val  = parameters['g']    
    
    # compute numerical coefficients of jacobian at stationary point
    jacobian_A = jacobian_A.subs([(l1, l1_val), (l2, l2_val), (d1, d1_val), (d2, d2_val), (m1, m1_val), (m2, m2_val), (g, g_val)])
    jacobian_numerical_A = jacobian_A.subs([(y1, ys[0]), (y2, ys[1]), (y3, ys[2]), (y4, ys[3]), (y5, ys[4]), (y6, ys[5]), (y7, ys[6]), (y8, ys[7]), (M1, M1s), (M2, M2s), (Fsupx, Fsupxs), (Fsupz, Fsupzs)])
    
    jacobian_B = jacobian_B.subs([(l1, l1_val), (l2, l2_val), (d1, d1_val), (d2, d2_val), (m1, m1_val), (m2, m2_val), (g, g_val)])
    jacobian_numerical_B = jacobian_B.subs([(y1, ys[0]), (y2, ys[1]), (y3, ys[2]), (y4, ys[3]), (y5, ys[4]), (y6, ys[5]), (y7, ys[6]), (y8, ys[7]), (M1, M1s), (M2, M2s), (Fsupx, Fsupxs), (Fsupz, Fsupzs)])    
    
    # convert to numpy array
    jacobian_numpy_array_A = numpy.array(jacobian_numerical_A.tolist()).astype(numpy.float64)
    jacobian_numpy_array_B = numpy.array(jacobian_numerical_B.tolist()).astype(numpy.float64)
    
    return jacobian_numpy_array_A, jacobian_numpy_array_B
    
parameters = {'l1': 0.3, 'l2': 0.3, 'd1': -0.4, 'd2': -0.4, 'm1': 3, 'm2': 2, 'g': 10}
x1s = [0.0359, 0, -0.1457, 0, 0.1868, 0, -0.3876, 0]
M1s = -1.20
M2s = 2.30
Fsupxs = 0
Fsupzs = 0

A, B = compute_jacobian_at_stationary_point(x1s, M1s, M2s, Fsupxs, Fsupzs, parameters)

print(A)
print(B)