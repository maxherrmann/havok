from numpy import linspace
from numpy import array
from numpy import concatenate
from numpy import shape
from numpy import interp
from numpy import diag

from numpy.linalg import svd
from numpy.linalg import pinv

import matplotlib.pyplot as plotter

import double_pendulum_free as dp_free
import double_pendulum_lqr as dp_lqr
import input_data

# simulation time
t_end = 40
dt = 0.03

# parameter values
l1 = 0.3
l2 = 0.3
d1 = -0.4
d2 = -0.4
m1 = 3
m2 = 2
g  = 10

# parameters for free double pendulum dynamics
parameters = {'l1': l1, 'l2': l2, 'm1': m1, 'm2': m2, 'g': g, 'd1': d1, 'd2': d2}

# initial conditions
x0 = [0.0359, 0, -0.1457, 0, 0.1868, 0, -0.3876, 0]

# read input data
path_to_input_data_file = '../data/XSENSOR_pressure_mat/A1C2.csv'
u_data = input_data.preprocess_input_data(path_to_input_data_file, 640, -1)
number_of_input_values = len(u_data)
input_time_vector = linspace(0, t_end, number_of_input_values)

# create input data structure
u = {'t': input_time_vector, 'y': u_data}

# call solver for free double pendulum dynamics
dp_free_out = dp_free.solve(x0, dt, t_end, u, parameters, False)

# LQR gains
# Q = eye(8);
# R = 2 * eye(2);
# N = zeros(8, 2);
# linearized about xs = [0.0359, 0, -0.1457, 0, 0.1868, 0, -0.3876, 0], M1s = -1.2, M2s = 2.3
k1 = array([4.2688,   52.2841,   -0.7986, -113.2035,   -2.1666,  -13.6122,    0.1229,   11.5961])
k2 = array([-41.5224, -227.0663,   39.0398,  179.9405,   23.7316,  116.1131,  -19.3953,  -95.6565])

# parameters for lqr-controlled double pendulum dynamics
parameters_lqr = {'l1': l1, 'l2': l2, 'm1': m1, 'm2': m2, 'g': g, 'd1': d1, 'd2': d2, 'k1': k1, 'k2': k2, 'xs': x0}

# call solver for free double pendulum dynamics
dp_lqr_out = dp_lqr.solve(x0, dt, t_end, u, parameters_lqr, False)

# create Hankel matrix of x2 - free dynamics
# dimensions
number_of_time_points = len(dp_free_out['t'])
p = (number_of_time_points * 3) // 4
q = number_of_time_points - p + 1
# append time-shifted states
H_free = array([dp_free_out['x2'][0:p]])
for row_index in range(1, q):
    H_free = concatenate((H_free, array([dp_free_out['x2'][row_index :(p + row_index)]])))
# append time-shifted inputs
u1 = dp_free_out['u1']
u2 = dp_free_out['u2']
for row_index in range(0, q):
    H_free = concatenate((H_free, array([u1[row_index :(p + row_index)]])))
    H_free = concatenate((H_free, array([u2[row_index :(p + row_index)]])))
    
# compute SVD of Hankel matrix
[U, S, VH] = svd(H_free, full_matrices=False)

# truncate singular value decomposition
r = 5
U_tilde = U[:, :r]
S_tilde = S[:r]
VH_tilde = VH[:r, :]

# DMDc on VH_tilde
X = VH_tilde[:, :-1]
X_next = VH_tilde[:, 1:]
Lambda = X_next @ pinv(X)
pinvUS = pinv(U_tilde @ diag(S_tilde))
A = (U_tilde @ diag(S_tilde)) @ (Lambda @ pinvUS)
A_tilde = A[:q, :q]
B_tilde = A[:q, q:]

# test surrogate model on training data
x2_ext = dp_free_out['x2'][:q]
u_ext = []
for row_index in range(0, q):
    u_ext.append(u1[row_index])
    u_ext.append(u2[row_index])

number_of_integration_points = number_of_time_points - q
x2_lin = []
for time_index in range(0, number_of_integration_points):
    x2_ext_next = A_tilde @ array(x2_ext).T + B_tilde @ array(u_ext).T
    x2_ext = x2_ext_next
    x2_lin.append(x2_ext_next[-1])

# end_index = len(dp_free_out['t']) - 1
end_index = 200
plotter.plot(dp_free_out['t'][q:q + end_index], dp_free_out['x2'][q: q + end_index])
plotter.plot(dp_free_out['t'][q:q + end_index], x2_lin[:end_index])
plotter.xlabel('t [s]')
plotter.ylabel('forearm x [m]')
plotter.legend(["nonlinear", "linear (r = {:d})".format(r)])


plotter.semilogy(S, "1", color='red')
axis = plotter.gca()
axis.set_xlim([-1, 20])
axis.set_ylim([10**-2, 10**3])
plotter.xlabel('singular value index [1]')
plotter.ylabel('singular value $\sigma$ [1]')

# compare free vs lqr dynamics
# (t, x2)
plotter.subplot(211)
plotter.plot(dp_free_out['t'], dp_free_out['x2'])
plotter.plot(dp_lqr_out['t'], dp_lqr_out['x2'])
plotter.ylabel('forearm x [m]')
plotter.legend(['free', 'LQR'], loc='lower left')
axis = plotter.gca()
axis.set_xlim([0, t_end])
plotter.subplot(212)
plotter.plot(input_time_vector, u_data)
plotter.xlabel('t [s]')
plotter.ylabel('pressure mat integral load [N]')
plotter.xlim([input_time_vector[0], input_time_vector[-1]])
# (x2, z2)
plotter.figure()
plotter.plot(dp_free_out['x2'], dp_free_out['z2'])
# plotter.plot(dp_lqr_out['x2'], dp_lqr_out['z2'])
plotter.xlabel('forearm x [m]')
plotter.ylabel('forearm z [m]')
# plotter.legend(['free', 'LQR'])
axis = plotter.gca()
axis.set_aspect('equal')

plotter.show()