import sympy

def compute_lagrange_multipliers():

    # define symbols
    f1t = sympy.symbols('f1t', cls=sympy.Function)
    f2t = sympy.symbols('f2t', cls=sympy.Function)
    f1, f2 = sympy.symbols('f1, f2')
    t = sympy.symbols('t')
    x1t, z1t, x2t, z2t = sympy.symbols('x1t, z1t, x2t, z2t', cls=sympy.Function)
    x1, z1, x2, z2 = sympy.symbols('x1, z1, x2, z2')
    dx1, dz1, dx2, dz2 = sympy.symbols('dx1, dz1, dx2, dz2')
    l1, l2, m1, m2, g = sympy.symbols('l1, l2, m1, m2, g')
    cosphi1, sinphi1, cosphi2, sinphi2 = sympy.symbols('cosphi1, sinphi1, cosphi2, sinphi2')
    # applied forces
    M1, M2 = sympy.symbols('M1, M2')
    FM1, FM2, FM1x, FM1z, FM2x, FM2z = sympy.symbols('FM1, FM2, FM1x, FM1z, FM2x, FM2z')
    d1, d2 = sympy.symbols('d1, d2')
    Fd1x, Fd1z, Fd2x, Fd2z = sympy.symbols('Fd1x, Fd1z, Fd2x, Fd2z')
    Fg1z, Fg2z = sympy.symbols('Fg1x, Fg2z')
    Fsupx, Fsupz = sympy.symbols('Fsupx, Fsupz')
    # constraint forces
    lambda1, lambda2 = sympy.symbols('lambda1, lambda2')
    sum_of_all_constraint_forces_x1 = sympy.symbols('sum_of_all_constraint_forces_x1')
    sum_of_all_constraint_forces_z1 = sympy.symbols('sum_of_all_constraint_forces_z1')
    sum_of_all_constraint_forces_x2 = sympy.symbols('sum_of_all_constraint_forces_x2')
    sum_of_all_constraint_forces_z2 = sympy.symbols('sum_of_all_constraint_forces_z2')

    # define constraints
    f1t = x1t(t)**2 + z1t(t)**2 - (l1/2)**2
    f2t = (x2t(t) - 2 * x1t(t))**2 + (z2t(t) - 2 * z1t(t))**2 - (l2/2)**2

    # 2nd derivative over time of constraints
    DDt_f1 = sympy.diff(f1t, t, 2)
    DDt_f2 = sympy.diff(f2t, t, 2)

    # cosines and sines of angle as a function of coordinates
    cosphi1 = (-z1) / (l1/2)
    sinphi1 = -(x1 / (l1/2))
    cosphi2 = -(z2 - 2*z1) / (l2/2)
    sinphi2 = -(x2 - 2*x1) / (l2/2)

    # rotation matrices
    T1 = sympy.Matrix([[cosphi1, sinphi1], [-sinphi1, cosphi1]])
    T2 = sympy.Matrix([[cosphi2, sinphi2], [-sinphi2, cosphi2]])

    # torque-induced forces
    FM1 = T1 * sympy.Matrix([M1 / (l1/2), 0])
    FM1x = FM1[0]
    FM1z = FM1[1]
    FM2 = T2 * sympy.Matrix([M2 / (l2/2), 0])
    FM2x = FM2[0]
    FM2z = FM2[1]
    
    # cosines and sines of angular velocity as a function of coordinates
    cosdphi1 = (-dz1) / (l1/2)
    sindphi1 = -(dx1 / (l1/2))
    cosdphi2 = -(dz2 - 2*dz1) / (l2/2)
    sindphi2 = -(dx2 - 2*dx1) / (l2/2)

    # damping forces
    Fd1x = -d1 * sindphi1
    Fd1z = -d1 * cosdphi1
    Fd2x = -d2 * sindphi2
    Fd2z = -d2 * cosdphi2

    # gravitational force
    Fg1z = -m1 * g
    Fg2z = -m2 * g

    # constraint forces
    f1 = f1t.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])
    f2 = f2t.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])
    df1dx1 = sympy.diff(f1, x1, 1)
    df2dx1 = sympy.diff(f2, x1, 1)
    df1dz1 = sympy.diff(f1, z1, 1)
    df2dz1 = sympy.diff(f2, z1, 1)  
    df1dx2 = sympy.diff(f1, x2, 1)
    df2dx2 = sympy.diff(f2, x2, 1)
    df1dz2 = sympy.diff(f1, z2, 1)
    df2dz2 = sympy.diff(f2, z2, 1)
    sum_of_all_constraint_forces_x1 = lambda1 * df1dx1 + lambda2 * df2dx1
    sum_of_all_constraint_forces_z1 = lambda1 * df1dz1 + lambda2 * df2dz1
    sum_of_all_constraint_forces_x2 = lambda1 * df1dx2 + lambda2 * df2dx2
    sum_of_all_constraint_forces_z2 = lambda1 * df1dz2 + lambda2 * df2dz2

    # laws of motion (right-hand side)
    DDt_x1 = (FM1x + Fd1x + Fsupx + sum_of_all_constraint_forces_x1) / m1
    DDt_z1 = (FM1z + Fd1z + Fsupz + Fg1z + sum_of_all_constraint_forces_z1) / m1
    DDt_x2 = (FM2x + Fd2x + sum_of_all_constraint_forces_x2) / m2
    DDt_z2 = (FM2z + Fd2z + Fg2z + sum_of_all_constraint_forces_z2) / m2

    # substitute 2nd total time derivatives with laws of motion
    DDt_f1 = DDt_f1.subs(
         [(sympy.diff(x1t(t), t, 2), DDt_x1), (sympy.diff(z1t(t), t, 2), DDt_z1), (sympy.diff(x2t(t), t, 2), DDt_x2), (sympy.diff(z2t(t), t, 2), DDt_z2)]
         )
    DDt_f2 = DDt_f2.subs(
         [(sympy.diff(x1t(t), t, 2), DDt_x1), (sympy.diff(z1t(t), t, 2), DDt_z1), (sympy.diff(x2t(t), t, 2), DDt_x2), (sympy.diff(z2t(t), t, 2), DDt_z2)]
         )
         
    # substitute functions with symbols
    DDt_f1 = DDt_f1.subs([(sympy.diff(x1t(t), t, 1), dx1), (sympy.diff(z1t(t), t, 1), dz1), (sympy.diff(x2t(t), t, 1), dx2), (sympy.diff(z2t(t), t, 1), dz2)])
    DDt_f1 = DDt_f1.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])
    DDt_f2 = DDt_f2.subs([(sympy.diff(x1t(t), t, 1), dx1), (sympy.diff(z1t(t), t, 1), dz1), (sympy.diff(x2t(t), t, 1), dx2), (sympy.diff(z2t(t), t, 1), dz2)])
    DDt_f2 = DDt_f2.subs([(x1t(t), x1), (z1t(t), z1), (x2t(t), x2), (z2t(t), z2)])

    # solve for Lagrange multipliers
    lambda12 = sympy.linsolve([DDt_f1, DDt_f2], (lambda1, lambda2))

    # collect components of solution vector
    lambdas = []
    for solutions in lambda12:
        for solution in solutions:
            lambdas.append(solution)

    # lambdify Lagrange multiplier expression
    lambda1_val = sympy.lambdify([Fsupx, Fsupz, M1, M2, x1, dx1, z1, dz1, x2, dx2, z2, dz2, d1, d2, l1, l2, m1, m2, g], lambdas[0])
    lambda2_val = sympy.lambdify([Fsupx, Fsupz, M1, M2, x1, dx1, z1, dz1, x2, dx2, z2, dz2, d1, d2, l1, l2, m1, m2, g], lambdas[1])
    
    # lambdify constraint forces
    cf_x1 = sympy.lambdify([x1, z1, x2, z2, l1, l2, lambda1, lambda2], sum_of_all_constraint_forces_x1)
    cf_z1 = sympy.lambdify([x1, z1, x2, z2, l1, l2, lambda1, lambda2], sum_of_all_constraint_forces_z1)
    cf_x2 = sympy.lambdify([x1, z1, x2, z2, l1, l2, lambda1, lambda2], sum_of_all_constraint_forces_x2)
    cf_z2 = sympy.lambdify([x1, z1, x2, z2, l1, l2, lambda1, lambda2], sum_of_all_constraint_forces_z2)

    return lambda1_val, lambda2_val, cf_x1, cf_z1, cf_x2, cf_z2