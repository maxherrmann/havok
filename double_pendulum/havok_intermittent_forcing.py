from numpy import linspace
from numpy import array
from numpy import concatenate
from numpy import shape
from numpy import interp
from numpy import diag

from numpy.linalg import svd
from numpy.linalg import pinv

import matplotlib.pyplot as plotter

import double_pendulum_free as dp_free
import double_pendulum_lqr as dp_lqr
import input_data

# simulation time
t_end = 40
dt = 0.03

# parameter values
l1 = 0.3
l2 = 0.3
d1 = -0.4
d2 = -0.4
m1 = 3
m2 = 2
g  = 10

# parameters for free double pendulum dynamics
parameters = {'l1': l1, 'l2': l2, 'm1': m1, 'm2': m2, 'g': g, 'd1': d1, 'd2': d2}

# initial conditions
x0 = [0.0359, 0, -0.1457, 0, 0.1868, 0, -0.3876, 0]

# constant input
u = {'t': [0, t_end], 'y': [1, 1]}

# call solver for free double pendulum dynamics
dp_free_out = dp_free.solve(x0, dt, t_end, u, parameters, False)

# LQR gains
# Q = eye(8);
# R = 2 * eye(2);
# N = zeros(8, 2);
# linearized about xs = [0.0359, 0, -0.1457, 0, 0.1868, 0, -0.3876, 0], M1s = -1.2, M2s = 2.3
k1 = array([4.2688,   52.2841,   -0.7986, -113.2035,   -2.1666,  -13.6122,    0.1229,   11.5961])
k2 = array([-41.5224, -227.0663,   39.0398,  179.9405,   23.7316,  116.1131,  -19.3953,  -95.6565])

# parameters for lqr-controlled double pendulum dynamics
parameters_lqr = {'l1': l1, 'l2': l2, 'm1': m1, 'm2': m2, 'g': g, 'd1': d1, 'd2': d2, 'k1': k1, 'k2': k2, 'xs': x0}

# call solver for free double pendulum dynamics
dp_lqr_out = dp_lqr.solve(x0, dt, t_end, u, parameters_lqr, False)

# create Hankel matrix of x2 - free dynamics
# dimensions
number_of_time_points = len(dp_free_out['t'])
p = (number_of_time_points * 3) // 4
q = number_of_time_points - p + 1
# append time-shifted states
H_free = array([dp_free_out['x2'][0:p]])
for row_index in range(1, q):
    H_free = concatenate((H_free, array([dp_free_out['x2'][row_index :(p + row_index)]])))
    
# compute SVD of Hankel matrix
[U, S, VH] = svd(H_free, full_matrices=False)

# DMD on truncated right singular vectors
r = 5
V = VH.T
X = V[:-1, :r]
X_next = V[1:, :r]
Xi = X_next @ pinv(X)
A = Xi[:(r-1), :(r-1)]
b = Xi[:(r-1), r]

# test surrogate model with intermittent forcing on training data in pseudo coordinates
# initial conditions
v = array(V[0, :r-1]).T
number_of_integration_points = number_of_time_points - q
v_sim = array([v])
for time_index in range(0, number_of_integration_points):
    v_next = A @ v + b * V[time_index, r]
    v = v_next
    v_sim = concatenate((v_sim, array([v_next])))

plotter.plot(v_sim[:, 0])

plotter.figure()
plotter.plot(V[:, r])

# delay embedding phase plot
plt3d = plotter.figure().gca(projection='3d')
plt3d.plot(V[:, 0], V[:, 1], V[:, 2])

# reconstructed delay embedding phase plot
plt3d.plot(v_sim[:, 0], v_sim[:, 1], v_sim[:, 2])

plotter.show()

plotter.semilogy(S, "1", color='red')
axis = plotter.gca()
axis.set_xlim([-1, 20])
axis.set_ylim([10**-2, 10**3])
plotter.xlabel('singular value index [1]')
plotter.ylabel('singular value $\sigma$ [1]')

# compare free vs lqr dynamics
# (t, x2)
plotter.plot(dp_free_out['t'], dp_free_out['x2'])
plotter.plot(dp_lqr_out['t'], dp_lqr_out['x2'])
plotter.xlabel('t [s]')
plotter.ylabel('forearm x [m]')
plotter.legend(['free', 'LQR'])
axis = plotter.gca()
axis.set_xlim([0, t_end])
# (x2, z2)
plotter.figure()
plotter.plot(dp_free_out['x2'], dp_free_out['z2'])
plotter.plot(dp_lqr_out['x2'], dp_lqr_out['z2'])
plotter.xlabel('forearm x [m]')
plotter.ylabel('forearm z [m]')
plotter.legend(['free', 'LQR'])
axis = plotter.gca()
axis.set_aspect('equal')