import numpy

from scipy.integrate import solve_ivp as solver

import matplotlib.pyplot as plotter
import matplotlib.animation as animation

import lagrange_multiplier as L
import rhs

def deg2rad(arg):
    return arg * numpy.pi / 180

# rotation matrix
def T(phi):
    return numpy.array([[numpy.cos(phi), -numpy.sin(phi)], [numpy.sin(phi), numpy.cos(phi)]])

# shoulder torque
def M1(t):
    # if t < 5:
        # return t/5 * (-1.2)
    return -1.2
    
def M2(t):
    # if t < 5:
        # return t/5 * 2.3
    return 2.3

# support vector
def Fsupx(t, nx, time_vector, data_vector):
    return nx * numpy.interp(t, time_vector, data_vector)
    
def Fsupz(t, nz, time_vector, data_vector):
    return nz * numpy.interp(t, time_vector, data_vector)

def solve(x0, dt, t_end, u, parameters, plot_results):

    # get Lagrange multiplier expression, and constraint forces
    lambda1_val, lambda2_val, cf_x1, cf_z1, cf_x2, cf_z2 = L.compute_lagrange_multipliers()   

    # parameters that need to be passed to the right-hand side
    rhs_parameters = (lambda1_val, lambda2_val, cf_x1, cf_z1, cf_x2, cf_z2, M1, M2, Fsupx, Fsupz, u, parameters)

    # integration time
    t_span = (0, t_end)
    time_points = numpy.arange(0, t_end, dt)
    number_of_time_points = len(time_points)

    # solve initial-value problem
    integration_method = 'Radau'
    solution = solver(rhs.f, t_span, x0, args=rhs_parameters, t_eval=time_points, method=integration_method)
    
    # compute support
    u1 = []
    u2 = []
    for time_index in range(0, len(solution.t)):
        t  = solution.t[time_index]
        z1 = solution.y[2][time_index]
        x1 = solution.y[0][time_index]
        l1 = parameters['l1']
        l2 = parameters['l2']
        nx = (-z1)/(l1/2)
        nz = x1/(l1/2)
        u1_val = Fsupx(t, nx, u['t'], u['y'])
        u2_val = Fsupz(t, nz, u['t'], u['y'])
        u1.append(u1_val)
        u2.append(u2_val)

    # create solution structure
    out = {'t': solution.t, 'x1': solution.y[0], 'dx1': solution.y[1], 'z1': solution.y[2], 'dz1': solution.y[3], 'x2': solution.y[4], 'dx2': solution.y[5], 'z2': solution.y[6], 'dz2': solution.y[7], 'u1': u1, 'u2': u2}

    # plot yes/no
    if not plot_results:
        return out

    # get mass 1 and 2 positions
    x1 = solution.y[0]
    z1 = solution.y[2]
    x2 = solution.y[4]
    z2 = solution.y[6]

    # plot results
    l1 = parameters['l1']
    l2 = parameters['l2']
    l = l1 + l2
    figure = plotter.figure(figsize = (5,4))
    axis = figure.add_subplot(autoscale_on=False, xlim=(-l, l), ylim=(-l, l))
    axis.set_aspect('equal')

    limbs, = axis.plot([], [])
    mass_1, = axis.plot([], [], 'o', color='red')
    mass_2, = axis.plot([], [], 'o', color='red')

    plotter.figure()
    plotter.plot(time_points, x2)

    # redraw limbs and masses at each time step
    def animate(frame_index):
        
        # draw limbs
        limbs.set_data(
           [0, 2*x1[frame_index], 2*(x2[frame_index] - x1[frame_index])], 
           [0, 2*z1[frame_index], 2*(z2[frame_index] - z1[frame_index])])
           
        # draw mass 1
        mass_1.set_data(x1[frame_index], z1[frame_index])
        
        # draw mass 2
        mass_2.set_data(x2[frame_index], z2[frame_index])
        
        return limbs, mass_1, mass_2
        
    animation_object = animation.FuncAnimation(figure, animate, number_of_time_points, interval=dt*1000, blit=True)

    plotter.show()

    return out