1. Messbereich
1.1 Sensorbereich: 5"x10"=12.7x25.4cm
1.2 tatsächlicher Messbereich = die Fläche der Armschale: 13x14cm = 5"x5.5"

2. Messfrequenz: 
2.1 alle Messungen wurde mit einer Geschwindigkeit von 20 Frames/s eingestellt, aber mit berechnet ca. 36-37 Frames/s aufgenommen. Die Dauer der Messung stimmt nicht mit andere Messdaten und Videoaufnahme. 
--> Die Ursache wrid noch von Hersteller erklärt.  

2.2 Validierungsmessung am 11.01.2022 mit Softwareversion 2020:
mit recording duration 5 Sekunde, die tatsächliche Messdauer simmit auch. Aber wenn die Messung nach 5 s manuel gestoppt wird, es zeigt sich nicht mit 20 Frames/s, sonder 34Frames/s

2.3 mit die Softwareversion 2021 wurde das Problem teilweise aber nicht ganz aufgehoben: die Messfrequenz wurde bis zum 20Hz eingestränkt.

3. Datenanalyse
3.1 das erste Zyklus sind meisten untypisch und werden bein der Berechnung nicht berücktichtigt.
die unvöllständige Zyklen (Aufnahme unvollständig oder die Aufgabe nicht fertig) wurden bereits in "Events validation.xslx" beseitigt:
	- P1, A1C3, 9-10te Wiederholung
	- P1, A2C2, 10te Wiederholung
	- P2, A2C2, 10te Wiederholung
	- P2, A2C3, 6,10-11te Wiederholung
	- P3, A1C3, 8-10te Wiederholung
	- P3, A2C2, 10te Wiederholung
	- P3, A2C3, 2,5-6,10te Wiederholung

3.2 Fehler beim Events?:
	- P1, A1C2, zweite Cycle_Start: ca. 2 Sekunde zu spät
	- P1, A1C3, fünfte Cycle_Start: ca. 3 Sekunde zu spät
	- P1, A2C3, vierte Cycle_Start: ca. 3 Sekunde zu spät
	- P2, A1C2, erste Cycle_Start: zu spät

3.3 Zero Pressure Filter wurde für alle Messungen (manche bei der Messung, manche bei der Nacharbeitung) verwendet, damit der Anfangszustand aller Messung als unbelastet bezeichnet. --> Die originale Messdaten werden trotzdembehalten und kann durch Sensel Transforms zurückgesetzt werden: Edit-->Sensel Transforms-->Applied-->ensprechendes "Transform" auswählen-->remove

3.2 Fehler beim Lucy? ausergewöhnliche Druck bei XSENSOR:
	- P2, A2C2, 1-4,9te Wiederholung
	- P2, A2C3, 4-9te Wiederholung
	- P3, A1C3, 3te Wiederholung
	- P3, A2C3, 7-8te Wiederholung
	- P5, Druckeinstellung prüfen, Poti untergedreht Während der Aufgaben?

